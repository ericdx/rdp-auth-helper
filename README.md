# RDP auth helper

WinForms app that fills RDP passwords for you. They are encrypted and stored only for your very local machine.

Usage:
1. Give your connections names in App.config
2. Launch app, set passwords for any of them. The app will store them encrypted.
3. Open RDP window, fill the exact address & port.
4. Click Auth! , the app will change focus to RDP window and press the Connect button, then fill your password for you.