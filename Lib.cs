﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace TrayFormsApp
{
    static class Lib
    {
        internal static List<string> procNames()
        {
           return Process.GetProcesses().Where(t => t.MainWindowTitle == "Подключение к удаленному рабочему столу").Select(p => p.MainWindowTitle).ToList();
        }
        
internal static IntPtr firstHandle()
        {
            return Process.GetProcesses().Where(t => t.MainWindowTitle == "Подключение к удаленному рабочему столу").Select(w => w.MainWindowHandle).FirstOrDefault();
        }
        public static string enCrypt(this string text)
        {
            return Convert.ToBase64String(
                ProtectedData.Protect(
                    Encoding.Unicode.GetBytes(text),null,DataProtectionScope.LocalMachine));
        }

        public static string Decrypt(this string text)
        {
            return Encoding.Unicode.GetString(
                ProtectedData.Unprotect(
                    Convert.FromBase64String(text), null, DataProtectionScope.LocalMachine));
        }

        internal static string prettify(string v)
        {
           return v.Replace(' ', '_');
        }
    }
}
