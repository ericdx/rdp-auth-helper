﻿namespace TrayFormsApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbProc = new System.Windows.Forms.ListBox();
            this.btnLocal = new System.Windows.Forms.Button();
            this.btnPwdSet = new System.Windows.Forms.Button();
            this.tbPwd = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbLog = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbProc
            // 
            this.rtbProc.FormattingEnabled = true;
            this.rtbProc.Location = new System.Drawing.Point(12, 12);
            this.rtbProc.Name = "rtbProc";
            this.rtbProc.Size = new System.Drawing.Size(458, 186);
            this.rtbProc.TabIndex = 3;
            // 
            // btnLocal
            // 
            this.btnLocal.Location = new System.Drawing.Point(482, 12);
            this.btnLocal.Name = "btnLocal";
            this.btnLocal.Size = new System.Drawing.Size(127, 35);
            this.btnLocal.TabIndex = 4;
            this.btnLocal.Text = "auth!";
            this.btnLocal.UseVisualStyleBackColor = true;
            this.btnLocal.Click += new System.EventHandler(this.btnAuth_Click);
            // 
            // btnPwdSet
            // 
            this.btnPwdSet.Location = new System.Drawing.Point(6, 19);
            this.btnPwdSet.Name = "btnPwdSet";
            this.btnPwdSet.Size = new System.Drawing.Size(75, 23);
            this.btnPwdSet.TabIndex = 5;
            this.btnPwdSet.Text = "set Pwd";
            this.btnPwdSet.UseVisualStyleBackColor = true;
            this.btnPwdSet.Click += new System.EventHandler(this.btnPwdSet_Click);
            // 
            // tbPwd
            // 
            this.tbPwd.Location = new System.Drawing.Point(6, 48);
            this.tbPwd.Name = "tbPwd";
            this.tbPwd.Size = new System.Drawing.Size(100, 20);
            this.tbPwd.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPwdSet);
            this.groupBox1.Controls.Add(this.tbPwd);
            this.groupBox1.Location = new System.Drawing.Point(476, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(133, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "change Pwd";
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.Location = new System.Drawing.Point(12, 260);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(458, 95);
            this.lbLog.TabIndex = 8;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(12, 204);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(130, 23);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add connection";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 409);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbLog);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnLocal);
            this.Controls.Add(this.rtbProc);
            this.Name = "Form1";
            this.Text = "RDP_Helper";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox rtbProc;
        private System.Windows.Forms.Button btnLocal;
        private System.Windows.Forms.Button btnPwdSet;
        private System.Windows.Forms.TextBox tbPwd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.Button btnAdd;
    }
}

