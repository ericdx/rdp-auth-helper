﻿using SettingsLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrayFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            rtbProc.Items.Clear();
            Settings.GetSplitted(Key.addresses).ForEach(p => rtbProc.Items.Add(p));

        }
       
        void auth(string pwd)
        {
 var hWnd = Lib.firstHandle();
            if (hWnd == IntPtr.Zero)
            {
                log("no rdp windows found");
                return;
            }
                SetForegroundWindow(hWnd);
            SendKeys.SendWait("{ENTER}");
            Thread.Sleep(2000);
            SendKeys.SendWait(pwd);
            SendKeys.SendWait("{ENTER}");
        }
     
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        //MoveWindow changes window's top, left, width and height...
        //...even if it do not want to change their dimensions and haves fixed edges!
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        private void btnAuth_Click(object sender, EventArgs e)
        {
            var rdp = selectedRdp;
            try
            {
                var pwd = Lib.Decrypt( File.ReadAllText($"pwd/{rdp}"));
                auth(pwd);
            } catch (FileNotFoundException)
            {
                log("set pass at first");
            }
        }

        private void log(string v)
        {
            lbLog.Items.Add(v);
        }

        public string selectedRdp => Lib.prettify(rtbProc.SelectedItem as string);
        private void btnPwdSet_Click(object sender, EventArgs e)
        {
            var rdp = selectedRdp;
            Directory.CreateDirectory($"pwd");
            File.WriteAllText($"pwd/{rdp}", Lib.enCrypt(tbPwd.Text));
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //todo: move list from app.config
        }
    }
}
