﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace TrayFormsApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
 Application.Run(new Form1());            

            //hWnd = FindWindow(null, "Calculator"); // Calculator is Windows calc title text            MoveWindow(hWnd, 100, 200, 300, 400, true);
        }

       
    }
}
